﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpHomework8Module16
{
    public class Fibonachi
    {
        public int[] AddFibonachi(int[] fibonachi)
        {

            int[] newFibonachi = new int[fibonachi.Count()];
            int count = 0;
            int step = fibonachi.Count();
            while (count != (fibonachi.Count()))
            {
                if (count == 0)
                {
                    newFibonachi[count] = fibonachi[fibonachi.Count() - 2] + fibonachi[fibonachi.Count() - 1];
                    count++;
                }
                else if (count == 1)
                {
                    newFibonachi[count] = newFibonachi[0] + fibonachi[fibonachi.Count() - 1];
                    count++;
                }
                else
                {
                    newFibonachi[count] = newFibonachi[count - 1] + newFibonachi[count - 2];
                    count++;
                }

            }
            return newFibonachi;
        }
    }
}
