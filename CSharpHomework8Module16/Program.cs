﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CSharpHomework8Module16
{
    class Program
    {
        static void Main(string[] args)
        {

            while (true)
            {
                Console.Clear();
                string m = "";
                Console.WriteLine("Press 1 to start task 1");
                Console.WriteLine("Press 2 to start task 2");
                m = Console.ReadLine();
                if (m == "1")
                {
                    Fibonachi fibonachi = new Fibonachi();
                    string fibonachiStr = "";
                    string path = "text.txt";

                    using (StreamReader sr = new StreamReader(path))
                    {
                        fibonachiStr = sr.ReadToEnd();
                    }

                    int[] intFibonachi = fibonachiStr.Split(' ').Select(i => Convert.ToInt32(i)).ToArray();

                    foreach (var item in intFibonachi)
                    {
                        Console.WriteLine(item + " ");
                    }

                    using (StreamWriter sw = File.AppendText(path))
                    {
                        fibonachiStr = " " + string.Join(" ", fibonachi.AddFibonachi(intFibonachi));
                        sw.Write(fibonachiStr);
                    }

                    using (StreamReader sr = new StreamReader(path))
                    {
                        Console.WriteLine(sr.ReadToEnd());
                    }
                    Console.ReadLine();
                }

                else if (m == "2")
                {
                    string numberStr = "";
                    string inputPath = "input.txt";
                    string outputPath = "output.txt";

                    using (StreamReader sr = new StreamReader(inputPath))
                    {
                        numberStr = sr.ReadToEnd();
                    }

                    int[] sumNumber = numberStr.Split(' ').Select(i => Convert.ToInt32(i)).ToArray();
                    int sum = sumNumber.Sum();

                    using (StreamWriter sw = new StreamWriter(outputPath))
                    {
                        sw.WriteLine(sum.ToString());
                    }

                    Console.ReadLine();
                }

            }




            Console.ReadLine();
        }
    }
}
